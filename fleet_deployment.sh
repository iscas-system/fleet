#!/bin/bash
set -e

# 环境检查函数
check_environment() {
    echo "++++++++++++++++++++++++"
    echo "  [1/5]执行环境检查...   "
    echo "++++++++++++++++++++++++"
    echo -e "\n"

    
    # 检查root权限
    if [ "$(id -u)" -ne 0 ]; then
        echo "错误：请使用root权限执行本脚本"
        exit 1
    fi

    # 检查OpenEuler版本
    if ! grep -q "openEuler 24.03 (LTS)" /etc/os-release; then
        echo "错误：本脚本仅支持openEuler 24.03"
        exit 1
    fi

    # 检查系统的库路径
    if ! ldconfig -v 2>/dev/null | grep -q "/usr/local/lib64"; then
        echo "/usr/local/lib64" >> /etc/ld.so.conf
        ldconfig
    fi

    echo "环境检查成功！！！"
    echo -e "\n"
}

# 安装Rust工具链
install_rust() {
    echo "++++++++++++++++++++++++"
    echo "  [2/5]安装Rust工具链... "
    echo "++++++++++++++++++++++++"
    echo -e "\n"

    # 检查 Rust 是否已经安装，并且版本是否符合要求
    if command -v rustc &>/dev/null && rustc --version | grep -q "1.83.0"; then
        echo "Rust 已安装，版本符合要求，无需重新安装。"
        echo -e "\n"
        return 0 # 返回函数
    fi

    echo "Rust 未安装或版本不匹配，开始安装..."

    export RUSTUP_DIST_SERVER=https://mirrors.ustc.edu.cn/rust-static
    export RUSTUP_UPDATE_ROOT=https://mirrors.ustc.edu.cn/rust-static/rustup
    
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | \
    sh -s -- --default-toolchain 1.83.0 -y
    
    source "$HOME/.cargo/env"
    
    if ! rustc --version | grep -q "1.83.0"; then
        echo "Rust版本检查失败"
        exit 1
    fi

    if ! cargo version | grep -q "1.83.0"; then
        echo "cargo版本检查失败"
        exit 1
    fi

    echo "安装Rust工具链成功！！！"
    echo -e "\n"    
}

# 安装系统依赖
install_dependencies() {
    echo "++++++++++++++++++++++++"
    echo "  [3/5]安装系统依赖...   "
    echo "++++++++++++++++++++++++"
    echo -e "\n"

    dnf groupinstall -y "Development Tools"
    dnf install -y pkgconf openssl-devel libuv-devel libuuid-devel \
        sqlite-devel postgresql-devel protobuf-compiler protobuf-devel
    
    echo "下载静态库..."
    # curl -L -o /usr/local/lib64/libfleet-datamgr-all-static.a \
    #     https://gitee.com/iscas-system/fleet-datamgr/releases/download/v0.2.0/libfleet-datamgr-all-static.a
    curl -L -o /usr/local/lib64/libfleet-datamgr-all-static.a https://g-ubjg5602-generic.pkg.coding.net/iscas-system/files/libfleet-datamgr-all-static-xquic.a?version=v0.5.0
    
    curl -L -o /usr/local/lib64/libxquic.so https://g-ubjg5602-generic.pkg.coding.net/iscas-system/files/libxquic.so

    echo "安装系统依赖成功！！！"
    echo -e "\n"
}

# 安装iSulad
install_isulad() {
    echo "++++++++++++++++++++++++"
    echo "  [4/5]安装iSulad...    "
    echo "++++++++++++++++++++++++"
    echo -e "\n"

    local WORK_DIR="/tmp/isulad"
    mkdir -p $WORK_DIR
    cd $WORK_DIR

    # 下载安装包
    ISULAD_ZIP="iSulad_rpm_v2.zip"
    if [ ! -f $ISULAD_ZIP ]; then
        wget https://gitee.com/iscas-system/fleet/releases/download/v0.0.1/$ISULAD_ZIP
        # wget https://gitee.com/iscas-system/fleet/releases/download/v0.0.1/iSulad_rpm_v2.zip
    fi

    # 解压安装
    unzip -o $ISULAD_ZIP -d .
    # unzip -o iSulad_rpm_v2.zip -d .

    # 进入解压后的目录
    ISULAD_DIR=$(basename "$ISULAD_ZIP" .zip)  # 提取文件夹名iSulad_rpm_v2
    # ISULAD_DIR=$(basename "iSulad_rpm_v2.zip" .zip)
    cd "$ISULAD_DIR" || { echo "解压目录不存在，退出！"; exit 1; }
    # 运行iSulad安装脚本
    chmod +x install.sh crictl.sh
    ./install.sh
    ./crictl.sh


    echo "iSulad daemon.json 配置检查..."
    CONFIG_FILE="/etc/isulad/daemon.json"

    # 确保 daemon.json 存在
    if [ ! -f "$CONFIG_FILE" ]; then
        echo "{}" > "$CONFIG_FILE"
    fi

    # 使用 sed 追加配置项（如果不存在）
    grep -q '"network-plugin":' "$CONFIG_FILE" || sed -i '/{/a\    "network-plugin": "cni",' "$CONFIG_FILE"
    grep -q '"cni-bin-dir":' "$CONFIG_FILE" || sed -i '/{/a\    "cni-bin-dir": "/opt/cni/bin",' "$CONFIG_FILE"
    # 注意：由于系统问题，daemon.json文件中最终生成/etc/cni.net.d需要手动修改 . 为 / 
    grep -q '"cni-conf-dir":' "$CONFIG_FILE" || sed -i '/{/a\    "cni-conf-dir": "/etc/cni/net.d",' "$CONFIG_FILE" 

    # grep -q '"network-plugin":' "$CONFIG_FILE" || sed -i '/{/a\    "network-plugin": "cni",' "$CONFIG_FILE"
    # grep -q '"cni-bin-dir":' "$CONFIG_FILE" || sed -i '/{/a\    "cni-bin-dir": "\/opt\/cni\/bin",' "$CONFIG_FILE"
    # grep -q '"cni-conf-dir":' "$CONFIG_FILE" || sed -i '/{/a\    "cni-conf-dir": "\/etc\/cni\/net.d",' "$CONFIG_FILE" # 注意：需要
    # # sed -i '/{/a\    "cni-conf-dir": "/etc/cni/net.d",' /etc/isulad/daemon.json"

    echo "iSulad daemon.json 配置检查并更新完成！"


    systemctl daemon-reload
    systemctl enable --now isulad
    sleep 5  # 等待服务启动

    echo "iSulad安装成功并启动成功！！！"
    echo -e "\n"
}


install_cni() {
    echo "++++++++++++++++++++++++"
    echo "  [5/5]安装CNI...       "
    echo "++++++++++++++++++++++++"
    echo -e "\n"

    local WORK_DIR="/tmp/cni"
    mkdir -p $WORK_DIR
    cd $WORK_DIR

    # 下载安装包
    CNI_RPM="nebula-cni-v0.1.6-9aae78f.oe2403.x86_64.rpm"
    echo "下载 CNI 插件 RPM 包..."
    if [ ! -f $ISULAD_ZIP ]; then
        wget https://gitee.com/iscas-system/fleet/releases/download/v0.0.1/$CNI_RPM
        # wget https://gitee.com/iscas-system/fleet/releases/download/v0.0.1/nebula-cni-v0.1.6-9aae78f.oe2403.x86_64.rpm
        # wget https://gitee.com/iscas-system/fleet/releases/download/v0.0.1/nebula-cni-v0.1.6-9aae78f.oe2403.x86_64.rpm
        
    fi
    # 下载失败
    
    echo "安装 CNI 插件..."
    rpm -ivh $CNI_RPM

    echo "安装完成，检查安装结果..."
    rpm -q nebula-cni || { echo "安装失败"; exit 1; }

    echo "CNI 插件已成功安装！！！"
    echo -e "\n"
}

# 主执行流程
main() {
    start_time=$(date +%s)  # 记录开始时间
        
    check_environment
    install_rust
    install_dependencies
    install_isulad
    install_cni

    echo "fleet部署环境配置完成！！！访问日志：/var/log/fleet-core.log"
    end_time=$(date +%s)  # 记录结束时间
    elapsed_time=$((end_time - start_time))  # 计算时间差
    echo "脚本运行时间: ${elapsed_time} 秒"
}

main "$@"