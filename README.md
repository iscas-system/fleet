# fleet

## 介绍
自研的边缘云平台，兼容 CRI（容器运行时接口） 和 CNI（容器网络接口）。


## 开发环境准备：

> openeuler 24.03

### 1.安装rust 1.83.0 版本
安装指定rust版本

```json
curl --proto '=https' --tlsv1.2 https://sh.rustup.rs -sSf | sh -s -- --default-toolchain 1.83.0
```

安装完成之后执行

```json
. "$HOME/.cargo/env" 
```

查看rustc版本

```json
[root@vm-002 ~]# rustc --version
rustc 1.83.0 (90b35a623 2024-11-26)
```

查看caogo版本

```json
[root@vm-002 ~]# cargo version
cargo 1.83.0 (5ffbef321 2024-10-29)
```

<font style="color:#DF2A3F;">备注：卸载rust：rustup self uninstall</font>

### 2.环境检查
**<font style="color:#DF2A3F;">注意：</font><font style="color:#DF2A3F;">务必</font><font style="color:#DF2A3F;">将/usr/local/lib64添加到系统的库路径中，请用下面命令进行检查</font>**

```json
[root@LAPTOP-SD01A7KT ~]# ldconfig -v 2>/dev/null | grep "/usr/local/lib64"
/usr/local/lib64: (from /etc/ld.so.conf:2)
```

### 3.安装依赖库
```json
dnf groupinstall "Development Tools"
dnf install pkgconf
dnf install openssl-devel
dnf install libuv-devel
dnf install libuuid-devel
dnf install sqlite-devel
dnf install libevent-devel
dnf install postgresql-devel
dnf install protobuf-compiler
dnf install protobuf-devel
curl -L -o /usr/local/lib64/libfleet-datamgr-all-static.a https://g-ubjg5602-generic.pkg.coding.net/iscas-system/files/libfleet-datamgr-all-static-xquic.a?version=v0.5.0
curl -L -o /usr/local/lib64/libxquic.so https://g-ubjg5602-generic.pkg.coding.net/iscas-system/files/libxquic.so
```
### 3.安装isulad

```json
下载操作系统提供的isulad安装包:wget https://gitee.com/iscas-system/fleet/releases/download/v0.0.1/iSulad_rpm_v2.zip
按照里面的说明文档进行安装
请注意:启动isulad之前请务必检查/etc/isulad/daemon.json 文件
{
        ....
"pod-sandbox-image": "registry.aliyuncs.com/google_containers/pause:3.5",
"native.umask": "normal",
"network-plugin": "cni",         ### 请务必加上cni，否则无法创建容器网络
"cni-bin-dir": "/opt/cni/bin",    ### 请务必加上/opt/cni/bin
"cni-conf-dir": "/etc/cni/net.d",  ### 请务必加上/etc/cni/net.d
"image-layer-check": false,
"use-decrypted-key": true,
"insecure-skip-verify-enforce": false,
"cri-runtimes": {
"kata": "io.containerd.kata.v2"
},
"enable-cri-v1": true
}


systemctl start isulad
如果下载镜像请用crictl pull xxxx

```
### 4.安装配置CNI插件

```
# 下载CNI的rpm安装包：
wget https://gitee.com/iscas-system/cni-hybrid/releases/download/0.1.5/nebula-cni-v0.1.5-9aae78f.oe2403.x86_64.rpm

# 安装CNI
rpm -ivh nebula-cni-v0.1.5-9aae78f.oe2403.x86_64.rpm

# 手动配置ip：可手动配置CNI插件项目的以下两个配置文件中的ip进行数据转发 (可选)
https://gitee.com/iscas-system/cni-hybrid/blob/master/examples/10-bridge.conf
https://gitee.com/iscas-system/cni-hybrid/blob/master/examples/20-macvlan.conf
```

### 5.安装fleet客户端fleetctl

```
# 下载fleet与fleetctl.tgz
wget https://gitee.com/iscas-system/fleet/releases/download/v0.0.1/fleetctl.tgz


# 解压fleetctl.tgz，该包中包含config、fleetctl文件和test（测试文件）文件夹
tar -xzf fleetctl.tgz -C fleetctl

# 配置fleet客户端fleetctl
mkdir /root/.fleet
mv config /root/.fleet
mv fleetctl /usr/local/bin/
mv test /root/test
```

### 6.编译运行fleet

- 用户可以直接git clone获取本项目，然后对fleet进行编译获取可执行文件；直接如果fleet编译通过，fleet依赖的组件apiserver,fleet-scheduler,fleet-controller,fleetlet,fleetmodv2,eventbus也都可以独立编译通过
- 可以直接通过以下命令在当前目录下载fleet可执行文件运行fleet

```
# 获取
wget https://gitee.com/iscas-system/fleet/releases/download/v0.0.1/fleet
# 赋予可执行权限
chomd +x fleet
# 一键启动fleet
RUST_BACKTRACE=1  ./target/debug/fleet  start core --local-message-server=::1-0-7000
```

### 7.pod与job的部署

- 进行pod部署前需获取clusterId并配置

  ```
  # 获取clusterId
  curl http://127.0.0.1:8080/cluster/info         
  
  {"status_code":"OK","message":"success","data":{"cluster_id":"f543aa5d-0954-4135-aab8-532965e0d724"}}
  fleetctl  create -f ./test/test-pod.yaml
  
  # 在/root/test/test-pod.yaml 的metadata 加上cluster字段
  apiVersion: v1
  kind: Pod
  metadata:
    name: http-cargo
    namespace: kube-system
    clusterId: f543aa5d-0954-4135-aab8-532965e0d724  // 加上clusterID
  ```

- 下载镜像

  ```
  crictl pull registry.cn-hangzhou.aliyuncs.com/liuliu-public/lw:latest
  ```

  

- pod的创建、更新、删除

  ```
  fleetctl  create -f ./test/test-pod.yaml
  
  crictl ps -a 查看部署的pod
  
  更新pod
  
  fleetctl  apply -f ./test/test-pod.yaml
  
  删除pod
  
  fleetctl delete pod <pod-name>
  ```

  

- job的创建、更新、删除

  ```
  # create
  fleetctl  create -f ./test/test-job.yaml
  
  # apply
  fleetctl  apply -f ./test/test-job.yaml
  
  # delete	
  fleetctl delete job <job-name>
  ```

  



### 8.独立测试apiserver

```rust
https://gitee.com/iscas-system/apiserver.git
RUST_BACKTRACE=1 RUST_LOG=trace cargo run --example basic_example --features="models eventbus servers test"
```

### 9.独立测试scheduler

```rust
https://gitee.com/iscas-system/scheduler.git
RUST_BACKTRACE=1 RUST_LOG=trace cargo run --example basic_example --features="models eventbus servers test"
```

### 10.独立测试controller

```rust
https://gitee.com/iscas-system/fleetlib-controller.git
RUST_BACKTRACE=1 RUST_LOG=trace cargo run --bin fleet-controller-start-example
```

### 11.独立测试fleetlet

```rust
https://gitee.com/iscas-system/fleetlet.git
RUST_LOG=trace cargo run --bin fleetlet-start-example 
```

