use clap::{Parser, Subcommand};

#[derive(Debug, Parser)]
#[command(
    name = "fleet",
    author = "Institute of Software, Chinese Academy of Sciences <jiangliuwei@iscas.ac.cn>",
    version = env!("CARGO_PKG_VERSION"),
    about = "A fleet management tool"
)]
pub struct Cli {
    #[command(subcommand)]
    pub command: Commands,
}

#[derive(Debug, Subcommand)]
pub enum Commands {
    /// Start the fleetcore service
    Start(StartCommand),

    /// Uninstall the fleet service
    Uninstall(UninstallCommand),
}

#[derive(Debug, Parser)]
pub struct StartCommand {
    #[command(subcommand)]
    pub start_command: StartSubcommands,
}

#[derive(Debug, Subcommand)]
pub enum StartSubcommands {
    /// Start the core service
    Core(CoreConfig),
    /// Start the agent service
    Agent(AgentConfig),
}

#[derive(Debug, Parser, Clone)]
pub struct CoreConfig {
    /// server configuration file path
    #[clap(long)]
    pub conf_file: Option<String>,
    /// stores the runtime,database,log data.
    #[clap(long, default_value = "/root/iscas/fleet")]
    pub data_dir: String,
    /// Logging level (e.g., debug, info, warn, error)
    #[clap(long, default_value = "info")]
    pub log_level: String,
    /// Address of the local message server
    #[clap(long)]
    pub local_message_server: String,
    /// Address of the another message server
    #[clap(long)]
    pub another_message_server: Vec<String>,
    /// [apiserver] server listening address
    #[clap(long, default_value = "0.0.0.0:8080")]
    pub address: String,
    /// [apiserver] database url
    #[clap(long, default_value = "sqlite:///root/iscas/fleet/db/fleet-database.sqlite")]
    pub database_url: String,

    /// [fleetlet] CRI daemon socket to connect for container runtime
    #[clap(long, default_value = "/var/run/isulad.sock")]
    pub container_runtime_endpoint: String,
    /// [fleetlet] CRI daemon socket to connect for image service
    #[clap(long, default_value = "/var/run/isulad.sock")]
    pub image_service_endpoint: String,
    #[clap(long, default_value = "http://127.0.0.1:8080")]
    pub api_server: String,
    /// Role of the node
    #[clap(long, default_value = "master")]
    pub role: String,
}

#[derive(Debug, Parser, Clone)]
pub struct AgentConfig {
    /// stores the runtime,database,log data.
    #[clap(long, default_value = "/root/iscas/fleetlet")]
    pub data_dir: String,
    /// Logging level (e.g., debug, info, warn, error)
    #[clap(long, default_value = "info")]
    pub log_level: String,
    /// If virtual node join cluster
    #[clap(long, default_value = "false")]
    pub vk: String,
    /// Path to the log directory
    #[clap(long)]
    pub log_path: Option<String>,
    /// Address of the ApiServer
    #[clap(long)]
    pub api_server: String,
    /// Role of the node
    #[clap(long, default_value = "work")]
    pub role: String,
    /// CRI daemon socket to connect for container runtime
    #[clap(long, default_value = "/var/run/isulad.sock")]
    pub container_runtime_endpoint: String,
    /// CRI daemon socket to connect for image service
    #[clap(long, default_value = "/var/run/isulad.sock")]
    pub image_service_endpoint: String,
    /// Configuration directory
    #[clap(long)]
    pub conf_dir: Option<String>,
    /// Address of the local message server
    #[clap(long)]
    pub local_message_server: String,
    /// Address of the another message server
    #[clap(long)]
    pub another_message_server: Vec<String>,
    /// nodename
    #[clap(long)]
    pub node_name: Option<String>,
}


#[derive(Debug, Parser)]
pub struct UninstallCommand {
    #[command(subcommand)]
    pub uninstall_command: UninstallSubcommands,
}

#[derive(Debug, Subcommand)]
pub enum UninstallSubcommands {
    /// Uninstall the core service
    Core(UninstallCoreConfig),
    /// Uninstall the agent service
    Agent(UninstallAgentConfig),
}

#[derive(Debug, Parser)]
pub struct UninstallCoreConfig {
    /// stores the runtime,database,log data.
    #[clap(long, default_value = "/root/iscas/fleet-core")]
    pub data_dir: Option<String>,
    #[clap(long)]
    pub api_server: String,
    /// Address of the local message server
    #[clap(long)]
    pub local_message_server: String,
    /// Address of the another message server
    #[clap(long)]
    pub another_message_server: Vec<String>,
}

#[derive(Debug, Parser)]
pub struct UninstallAgentConfig {
    /// Address of the ApiServer
    #[clap(long)]
    pub api_server: String,
    /// Name of the reset node
    #[clap(long)]
    pub node_name: String,
    /// Address of the local message server
    #[clap(long)]
    pub local_message_server: String,
    /// Address of the another message server
    #[clap(long)]
    pub another_message_server: Vec<String>,
}
#[allow(dead_code)]
fn validate_role(role: &str) -> Result<String, String> {
    match role {
        "master" | "worker" => Ok(role.to_string()),
        _ => Err(format!("Invalid role: {}. Must be 'master' or 'worker'", role)),
    }
}