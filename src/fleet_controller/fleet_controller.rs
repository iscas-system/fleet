use std::sync::Arc;
use feventbus::impls::messaging::messaging::Messaging;
use fleet_controller::cli::{StartCommand, Cli, Commands};
use crate::cli::CoreConfig;
use crate::util::ioerror::{IoError, IoResult};

pub struct FleetControllerManager {
    pub start_command: StartCommand,
}

impl FleetControllerManager {
    pub fn new_by_core_config(config: CoreConfig) -> Self {
        FleetControllerManager {
            start_command: StartCommand {
                conf_dir: "".to_string(),
                log_path: format!("{}/log/fleet-controller", config.data_dir),
                log_level: config.log_level,
                apiserver: config.api_server,
            },
        }
    }
    pub async fn run(self, msg_cli: Arc<Messaging>) -> IoResult<()> {
        let cli = Cli {
            command: Commands::Start(self.start_command),
        };
        fleet_controller::run(cli,msg_cli).await.map_err(|e| {
            log::error!("run fleetlet failed: {}", e);
            IoError::interrupted("run fleetlet failed", &e.to_string())
        })
    }
}
