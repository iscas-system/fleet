use std::sync::Arc;
use feventbus::impls::messaging::messaging::Messaging;
use crate::util::health;
use crate::util::ioerror::{IoError, IoResult};
use client_rust::ClientSet;


pub struct FleetSchedulerManager {
    pub api_server: String,
}


impl FleetSchedulerManager {
    pub fn new(api_server: String) -> Self {
        FleetSchedulerManager {
            api_server
        }
    }

    pub async fn run(self, msg_cli: Arc<Messaging>) -> IoResult<()> {
        let cluster_id = health::check_apiserver_service(self.api_server.as_str()).await.map_err(|e| {
            log::error!("get cluster id failed: {}", e);
            IoError::interrupted("get clusterid failed", &e.to_string())
        })?;
        let cli = ClientSet::events_with_cluster_id(
            cluster_id.clone().as_str(),
            msg_cli.clone(),
        ).await;
        fleet_scheduler::run_default(cli, cluster_id.as_str()).await;
        Ok(())
    }
}