use std::sync::Arc;
use std::time;
use feventbus::impls::messaging::messaging::Messaging;
use fleet_apiserver::cores::servers;
use fleet_apiserver::cores::servers::{MessagingServer, Server};
use fleet_apiserver::cores::state::AppState;
use fleet_apiserver::prepare_app_state;
use crate::cli::CoreConfig;
use crate::fleet_controller::fleet_controller::FleetControllerManager;
use crate::fleet_scheduler::fleet_scheduler::FleetSchedulerManager;
use crate::fleetlet::fleetlet::FleetletManager;
use crate::util::ioerror::IoResult;
use crate::util::message::{setup_message_cli, tear_down_message_cli};
use tokio::signal::unix::{signal, Signal, SignalKind};
const TCP_ADDRESS: &str = "0.0.0.0:8080";
const UDP_ADDRESS: &str = "0.0.0.0:38081";
const QUIC_ADDRESS: &str = "0.0.0.0:38082";

#[derive(Debug, Clone)]
pub struct CoreManager {
    pub config: CoreConfig,
}

impl CoreManager {
    pub fn new(config: CoreConfig) -> CoreManager {
        CoreManager {
            config
        }
    }

    pub async fn run(&self) -> IoResult<()> {
        let config = self.config.clone();
        let self_arc = Arc::new(self.clone());
        let message_client_arc = setup_message_cli(config.local_message_server.clone(), config.another_message_server.clone()).await?;

        // 启动fleetlet组件
        let message_client_move = message_client_arc.clone();
        tokio::spawn({
            let self_arc = Arc::clone(&self_arc);
            async move {
                let config = self_arc.config.clone();
                FleetletManager::new_by_core_config(config).run(message_client_move).await
            }
        });

        // 启动controller组件
        let message_client_move = message_client_arc.clone();
        tokio::spawn({
            let self_arc = Arc::clone(&self_arc);
            async move {
                let config = self_arc.config.clone();
                FleetControllerManager::new_by_core_config(config).run(message_client_move).await
            }
        });

        // 启动scheduler组件
        let message_client_move = message_client_arc.clone();
        let apiserver = config.api_server.clone();
        tokio::spawn({
            async move {
                FleetSchedulerManager::new(apiserver).run(message_client_move).await
            }
        });

        // 启动apiserver TODO:后续增加udp和quick服务启动参数
        let msg_cli_moved = message_client_arc.clone();
        let app_state = prepare_app_state(config.database_url.as_str(), msg_cli_moved).await.unwrap();
        let app_state_moved = app_state.clone();
        start_api_server(app_state_moved, TCP_ADDRESS.to_string(), None, None).await;

        // 优雅关闭
        await_shutdown_signal(message_client_arc).await;
        log::info!("CoreManager shutdown complete.");
        Ok(())
    }
}


async fn await_shutdown_signal(messaging_client: Arc<Messaging>) {
    // 使用 `signal` 来监听信号
    let mut sigterm = signal(SignalKind::terminate()).expect("Failed to set up SIGTERM listener");
    let mut sigint = signal(SignalKind::interrupt()).expect("Failed to set up SIGINT listener");
    println!("CoreManager is running, waiting for shutdown signal...");
    tokio::select! {
        _ = sigterm.recv() => {
            log::info!("Received SIGTERM signal.");
        }
        _ = sigint.recv() => {
            log::info!("Received SIGINT signal.");
        }
    }
    log::info!("Shutting down CoreManager...");
    tear_down_message_cli(messaging_client);
    log::info!("Cleaned up message client.");
}


#[allow(unused)]
pub async fn start_api_server(app_state: AppState, tcp_address: String, udp_address: Option<String>, quic_address: Option<String>) -> AppState {
    // 启动watch相关事件监听协程
    let app_state_watch = app_state.clone();
    tokio::spawn(async move {
        app_state_watch.watch_daemon.start().await;
    });

    log::info!("Starting test API server");
    // 启动各个server
    let messaging_server: Box<dyn Server> = Box::new(MessagingServer);
    let actix_web_tcp_server: Box<dyn Server> = servers::actix_web::tcp(tcp_address.as_str());
    let messaging_server_app_state = app_state.clone();
    tokio::spawn(async move {
        messaging_server.start(messaging_server_app_state).await;
    });
    let actix_web_tcp_server_app_state = app_state.clone();
    tokio::spawn(async move {
        let _ = actix_web_tcp_server.start(actix_web_tcp_server_app_state).await;
    });
    if let Some(udp_address) = udp_address {
        let actix_web_udp_server: Box<dyn Server> = servers::actix_web::udp(udp_address.as_str());
        let actix_web_udp_server_app_state = app_state.clone();
        tokio::spawn(async move {
            let _ = actix_web_udp_server.start(actix_web_udp_server_app_state).await;
        });
    }
    if let Some(quic_address) = quic_address {
        let actix_web_quic_server: Box<dyn Server> = servers::actix_web::quic(quic_address.as_str());
        let actix_web_quic_server_app_state = app_state.clone();
        tokio::spawn(async move {
            let _ = actix_web_quic_server.start(actix_web_quic_server_app_state).await;
        });
    }

    app_state
}