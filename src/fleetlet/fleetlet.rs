use std::sync::Arc;
use feventbus::impls::messaging::messaging::Messaging;
use fleetlet::cli::{Cli, Commands, ResetCommand, StartCommand};
use crate::cli::{CoreConfig, UninstallAgentConfig};
use crate::util::ioerror::{IoError, IoResult};

pub struct FleetletManager {
    pub start_command: StartCommand,
}

pub struct FleetInstallManager {
    pub reset_command: ResetCommand,
}

impl FleetletManager {
    pub fn new_by_core_config(config: CoreConfig) -> Self {
        FleetletManager {
            start_command: StartCommand {
                api_server: config.api_server,
                role: config.role,
                container_runtime_endpoint: config.container_runtime_endpoint,
                image_service_endpoint: config.image_service_endpoint,
                conf_dir: "".to_string(),
                log_path: format!("{}/log/fleetlet", config.data_dir),
                log_level: config.log_level,
                ip_file_path: None,
            },
        }
    }
    pub async fn run(self, msg_cli: Arc<Messaging>) -> IoResult<()> {
        let cli = Cli {
            command: Commands::Start(self.start_command),
        };
        fleetlet::run(cli, msg_cli).await.map_err(|e| {
            log::error!("run fleetlet failed: {}", e);
            IoError::interrupted("run fleetlet failed", &e.to_string())
        })
    }
}


impl FleetInstallManager {
    pub fn new_by_core_config(config: UninstallAgentConfig) -> Self {
        FleetInstallManager {
            reset_command: ResetCommand {
                api_server: config.api_server.to_string(),
                node_name: config.node_name.to_string(),
                nats_server: "".to_string(),
                nats_conf: None,
            }
        }
    }

    pub async fn run(self) -> IoResult<()> {
        let cli = Cli {
            command: Commands::Reset(self.reset_command),
        };
        log::info!("get cli {:#?}",cli);
        // fleetlet::run(cli, msg_cli).await.map_err(|e| {
        //     log::error!("uninstall fleetlet failed: {}", e);
        //     IoError::interrupted("uninstall fleetlet failed", &e.to_string())
        // })
        Ok(())
    }
}

