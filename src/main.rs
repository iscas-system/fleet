use clap::Parser;
use crate::agent::agent::AgentManager;
use crate::cli::{Commands, StartSubcommands, UninstallSubcommands};
use crate::core::core::CoreManager;
use crate::fleetlet::fleetlet::FleetInstallManager;
use crate::util::ioerror::IoResult;
use crate::util::util::{init_dir, init_fleet_logger};

pub mod cli;
mod fleetlet;
mod fleet_controller;
mod util;
mod core;
mod agent;

mod fleet_scheduler;


pub const COMPONENT: &str = "fleet";

#[tokio::main]
async fn main() -> IoResult<()> {
    let cli = cli::Cli::parse();

    match cli.command {
        Commands::Start(start_cmd) => match start_cmd.start_command {
            StartSubcommands::Core(config) => {
                init_dir(config.data_dir.as_str()).await?;
                init_fleet_logger(config.data_dir.as_str(), config.log_level.as_str()).await?;
                log::info!("{:#?}",config);
                CoreManager::new(config).run().await?;
                Ok(())
            }
            StartSubcommands::Agent(config) => {
                log::info!("Starting Fleet Agent service...");
                init_dir(config.data_dir.as_str()).await?;
                init_fleet_logger(config.data_dir.as_str(), config.log_level.as_str()).await?;
                log::info!("{:#?}",config);
                AgentManager::new(config).run().await?;
                Ok(())
            }
        },
        Commands::Uninstall(uninstall_cmd) => match uninstall_cmd.uninstall_command {
            UninstallSubcommands::Core(_config) => {
                log::info!("Uninstalling Fleet Core service...");
                Ok(())
                // core.run()?;
            }
            UninstallSubcommands::Agent(config) => {
                // log::info!("Uninstalling Fleet Agent service...");

                let _ = FleetInstallManager::new_by_core_config(config).run().await;

                Ok(())
                // fleetlet.run()?;
            }
        },
    }
}

