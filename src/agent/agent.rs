use crate::cli::{AgentConfig};
use fleetlet::cli::{Cli, Commands, VirtualCommand};
use crate::util::ioerror::{IoError, IoResult};
use crate::util::message::setup_message_cli;

#[derive(Debug, Clone)]
pub struct AgentManager {
    pub config: AgentConfig,
}


impl AgentManager {
    pub fn new(config: AgentConfig) -> Self {
        AgentManager {
            config
        }
    }


    pub async fn run(&self) -> IoResult<()> {

        let config = self.config.clone();
        let message_client_arc = setup_message_cli(config.local_message_server.clone(), config.another_message_server.clone()).await?;


        if self.config.vk == "false" {
            let start_command = fleetlet::cli::StartCommand {
                api_server: self.config.api_server.clone(),
                role: self.config.role.clone(),
                container_runtime_endpoint: self.config.container_runtime_endpoint.clone(),
                image_service_endpoint: self.config.image_service_endpoint.clone(),
                conf_dir: self.config.conf_dir.clone().unwrap_or_default(),
                log_path: self.config.log_path.clone().unwrap_or_default(),
                log_level: self.config.log_level.clone(),
                ip_file_path: None,
            };
            let cli = Cli {
                command: Commands::Start(start_command),
            };
            fleetlet::run(cli, message_client_arc).await.map_err(|e| IoError::other("start agentserver failed".to_string(), e.to_string()))?;

        }

        if self.config.node_name.is_none() {
            // return Err(IoError::without_other("please input --node-name"));
        }

        let vk_command = VirtualCommand {
            api_server: self.config.api_server.clone(),
            node_name: self.config.node_name.clone().unwrap(),
            nats_server: "".to_string(),
            nats_conf: None,
            ip_file_path: None,
        };

        log::info!("get vk_command:{:#?} !!!",vk_command);

        // let cli = Cli {
        //     command: Commands::Virtual(vk_command),
        // };

        // fleetlet::run(cli).await
        Ok(())
    }
}