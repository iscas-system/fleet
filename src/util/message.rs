use std::ffi::CString;
use std::sync::Arc;
use feventbus::impls::messaging::datamgr_api;
use feventbus::impls::messaging::messaging::Messaging;
use feventbus::traits::controller::EventBus;
use std::os::raw::{c_int, c_void};
use regex::Regex;
use crate::util::ioerror::{IoError, IoResult};

fn extract_parts(input: &str) -> Option<(CString, c_int, c_int, String)> {
    let re = Regex::new(r"^(([a-fA-F0-9:.]+)|localhost)-([0-9]+)-([0-9]+)$").unwrap();

    if let Some(caps) = re.captures(input) {
        let host_str = caps.get(1).map_or("", |m| m.as_str());
        let host = CString::new(host_str).ok()?;
        let num1: c_int = caps.get(3).map_or("", |m| m.as_str()).parse().ok()?;
        let num2: c_int = caps.get(4).map_or("", |m| m.as_str()).parse().ok()?;

        let ip_type = if host_str == "localhost" {
            "localhost"
        } else if host_str.contains(':') {
            "IPv6"
        } else {
            "IPv4"
        };

        return Some((host, num1, num2, ip_type.to_string()));
    }
    None
}


// pub async fn setup_message_cli(local_message_server: String, another_message_servers: Vec<String>) -> IoResult<Arc<Messaging>> {
//     let my_id = 0;
//
//     let data_plugin_manager = unsafe { datamgr_api::NewPluginManager(my_id) };
//
//     let plugin_to_load_key = CString::new("core.pluginsToLoad").unwrap();
//     let plugin_to_load_value = CString::new("Messaging Storage Portal").unwrap();
//
//
//     unsafe {
//         datamgr_api::SetParameter(
//             data_plugin_manager,
//             plugin_to_load_key.as_ptr(),
//             plugin_to_load_value.as_ptr(),
//         );
//         datamgr_api::LoadPlugins(data_plugin_manager);
//     }
//
//
//     let protocol = feventbus::impls::messaging::datamgr_api::PROTOCOL_IPV6;
//     let address = CString::new("::1").unwrap();
//     unsafe {
//         feventbus::impls::messaging::datamgr_api::StartUdp(
//             data_plugin_manager,
//             protocol as std::ffi::c_int,
//             address.as_ptr(),
//             7000,
//         );
//     }
//     let another_node_address = CString::new("localhost").unwrap();
//
//     unsafe {
//         datamgr_api::AddNodeToSync(
//             data_plugin_manager,
//             1,
//             another_node_address.as_ptr(),
//             7001,
//         );
//     }
//
//     tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
//     let mut messaging_client = Messaging::new().await.unwrap();
//     messaging_client.set_plugin_manager(data_plugin_manager);
//     let messaging_client_arc = Arc::new(messaging_client);
//     Ok(messaging_client_arc)
// }


#[allow(unused)]
pub async fn setup_message_cli(local_message_server: String, another_message_servers: Vec<String>) -> IoResult<Arc<Messaging>> {
    let (local_address, local_id, local_port, ip_type) = match extract_parts(local_message_server.as_str()) {
        None => { return Err(IoError::without_other("format local message server failed")) }
        Some(v) => { v }
    };

    let plugin_to_load_key = CString::new("core.pluginsToLoad").unwrap();
    let plugin_to_load_value = CString::new("Messaging Storage").unwrap();
    let leader_mq_plugin_manager = unsafe { init_mq_plugins(&plugin_to_load_key, &plugin_to_load_value) };

    let leader_address = CString::new("127.0.0.1").unwrap();
    unsafe {
        datamgr_api::StartUdp(
            leader_mq_plugin_manager,
            // protocol as c_int,
            leader_address.as_ptr(),
            // my_port,
        );
    }
    tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
    let mut messaging_client = Messaging::new().await.unwrap();
    messaging_client.set_plugin_manager(leader_mq_plugin_manager);
    let messaging_client_arc = Arc::new(messaging_client);
    Ok(messaging_client_arc)
}

#[allow(unused)]
pub fn tear_down_message_cli(messaging_client: Arc<Messaging>) {
    let plugin_manager = messaging_client.get_plugin_manager().expect("Plugin manager is not set");
    unsafe {
        datamgr_api::StopUdp(plugin_manager);
        datamgr_api::UnloadPlugins(plugin_manager);
        datamgr_api::DeletePluginManager(plugin_manager);
    }
}

unsafe fn init_mq_plugins(plugin_to_load_key: &CString, plugin_to_load_value: &CString) -> *mut c_void {
    let master_plugin_manager = datamgr_api::NewPluginManager();
    datamgr_api::SetParameter(
        master_plugin_manager,
        plugin_to_load_key.as_ptr(),
        plugin_to_load_value.as_ptr(),
    );

    datamgr_api::LoadPlugins(master_plugin_manager);
    master_plugin_manager
}
