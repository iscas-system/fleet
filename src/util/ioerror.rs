/**
* Copyright(2024,)Institute of Software, Chinese Academy of Sciences
* author: jiangliuwei@iscas.ac.cn
* since: 0.1.0
*/
use std::sync::{PoisonError, TryLockError};

#[derive(Debug)]
pub struct IoError {
    pub context: Option<String>,
    pub inner: std::io::Error,
}

impl Clone for IoError {
    fn clone(&self) -> Self {
        Self {
            context: self.context.clone(),
            inner: std::io::Error::new(self.inner.kind(), self.inner.to_string()),
        }
    }
}

impl IoError {
    pub fn new<T>(context: T, inner: std::io::Error) -> Self
    where
        T: Into<String>,
    {
        Self {
            context: Some(context.into()),
            inner,
        }
    }

    pub fn without_context(inner: std::io::Error) -> Self {
        Self {
            context: None,
            inner,
        }
    }

    pub fn invalid_data<M>(context: M, message: M) -> Self
    where
        M: ToString + std::fmt::Display,
    {
        Self::new(
            context.to_string(),
            std::io::Error::new(std::io::ErrorKind::InvalidData, message.to_string()),
        )
    }

    pub fn other<M>(context: M, message: M) -> Self
    where
        M: ToString + std::fmt::Display,
    {
        Self::new(
            context.to_string(),
            std::io::Error::new(std::io::ErrorKind::Other, message.to_string()),
        )
    }

    pub fn without_other<M>(message: M) -> Self
    where
        M: ToString + std::fmt::Display,
    {
        Self {
            context: None,
            inner: std::io::Error::new(std::io::ErrorKind::Other, message.to_string()),
        }
    }

    pub fn invalid_input<M>(context: M, message: M) -> Self
    where
        M: ToString + std::fmt::Display,
    {
        Self::new(
            context.to_string(),
            std::io::Error::new(
                std::io::ErrorKind::InvalidInput,
                message.to_string(),
            ),
        )
    }

    pub fn not_found<M>(context: M, message: M) -> Self
    where
        M: ToString + std::fmt::Display,
    {
        Self::new(
            context.to_string(),
            std::io::Error::new(std::io::ErrorKind::NotFound, message.to_string()),
        )
    }

    pub fn interrupted<M>(context: M, message: M) -> Self
    where
        M: ToString + std::fmt::Display,
    {
        Self::new(
            context.to_string(),
            std::io::Error::new(std::io::ErrorKind::Interrupted, message.to_string()),
        )
    }

    pub fn context(&self) -> Option<&str> {
        self.context.as_deref()
    }

    pub fn into_inner(self) -> std::io::Error {
        self.inner
    }

    pub fn exit(&self) -> ! {
        std::process::exit(self.inner.raw_os_error().unwrap_or(1));
    }

    pub fn print_and_exit(&self) -> ! {
        eprintln!("{}", self);
        self.exit();
    }
}

impl std::fmt::Display for IoError {
    fn fmt(
        &self,
        f: &mut std::fmt::Formatter<'_>,
    ) -> Result<(), std::fmt::Error> {
        use std::io::ErrorKind::*;

        let mut message;
        let message = if self.inner.raw_os_error().is_some() {
            // These are errors that come directly from the OS.
            // We want to normalize their messages across systems,
            // and we want to strip the "(os error X)" suffix.
            match self.inner.kind() {
                NotFound => "No such file or directory",
                PermissionDenied => "Permission denied",
                ConnectionRefused => "Connection refused",
                ConnectionReset => "Connection reset",
                ConnectionAborted => "Connection aborted",
                NotConnected => "Not connected",
                AddrInUse => "Address in use",
                AddrNotAvailable => "Address not available",
                BrokenPipe => "Broken pipe",
                AlreadyExists => "Already exists",
                WouldBlock => "Would block",
                InvalidInput => "Invalid input",
                InvalidData => "Invalid data",
                TimedOut => "Timed out",
                WriteZero => "Write zero",
                Interrupted => "Interrupted",
                UnexpectedEof => "Unexpected end of file",
                _ => {
                    message = strip_errno(&self.inner);
                    capitalize(&mut message);
                    &message
                }
            }
        } else {
            message = self.inner.to_string();
            capitalize(&mut message);
            &message
        };
        if let Some(ctx) = &self.context {
            write!(f, "{ctx}: {message}")
        } else {
            write!(f, "{message}")
        }
    }
}

impl std::error::Error for IoError {}

/// Capitalize the first character of an ASCII string.
pub fn capitalize(text: &mut str) {
    if let Some(first) = text.get_mut(..1) {
        first.make_ascii_uppercase();
    }
}

/// Strip the trailing " (os error XX)" from io error strings.
pub fn strip_errno(err: &std::io::Error) -> String {
    let mut msg = err.to_string();
    if let Some(pos) = msg.find(" (os error ") {
        msg.truncate(pos);
    }
    msg
}

pub type IoResult<T, E = IoError> = Result<T, E>;

/// Enables the conversion from [`std::io::Error`] to [`IoError`] and from [`std::io::Result`] to [`IoResult`].
#[allow(dead_code)]
pub trait FromIo<T> {
    fn map_err_context<C>(self, context: impl FnOnce() -> C) -> T
    where
        C: ToString + std::fmt::Display;
}

impl<T> FromIo<IoError> for TryLockError<T> {
    fn map_err_context<C>(self, context: impl FnOnce() -> C) -> IoError
    where
        C: ToString + std::fmt::Display,
    {
        IoError::interrupted((context)().to_string(), self.to_string())
    }
}

impl<T> From<TryLockError<T>> for IoError {
    fn from(e: TryLockError<T>) -> IoError {
        IoError::interrupted("Mutex", e.to_string().as_str())
    }
}

impl<T> FromIo<IoError> for PoisonError<T> {
    fn map_err_context<C>(self, context: impl FnOnce() -> C) -> IoError
    where
        C: ToString + std::fmt::Display,
    {
        IoError::interrupted((context)().to_string(), self.to_string())
    }
}

impl<T> From<PoisonError<T>> for IoError {
    fn from(e: PoisonError<T>) -> IoError {
        IoError::interrupted("Mutex", e.to_string().as_str())
    }
}

impl FromIo<IoError> for IoError {
    fn map_err_context<C>(self, context: impl FnOnce() -> C) -> IoError
    where
        C: ToString + std::fmt::Display,
    {
        IoError {
            context: Some((context)().to_string()),
            inner: self.into_inner(),
        }
    }
}

impl FromIo<Box<IoError>> for std::io::Error {
    fn map_err_context<C>(self, context: impl FnOnce() -> C) -> Box<IoError>
    where
        C: ToString + std::fmt::Display,
    {
        Box::new(IoError {
            context: Some((context)().to_string()),
            inner: self,
        })
    }
}

impl FromIo<Box<IoError>> for std::string::FromUtf8Error {
    fn map_err_context<C>(self, context: impl FnOnce() -> C) -> Box<IoError>
    where
        C: ToString + std::fmt::Display,
    {
        Box::new(IoError {
            context: Some((context)().to_string()),
            inner: std::io::Error::new(std::io::ErrorKind::InvalidData, self),
        })
    }
}

impl From<Box<IoError>> for IoError {
    fn from(f: Box<IoError>) -> Self {
        *f
    }
}

impl From<std::io::Error> for IoError {
    fn from(f: std::io::Error) -> Self {
        Self {
            context: None,
            inner: f,
        }
    }
}

impl From<IoError> for std::io::Error {
    fn from(f: IoError) -> Self {
        f.inner
    }
}



#[cfg(test)]
mod tests {
    use super::*; // 引入外部模块中的所有项

    #[test]
    fn test_io_error_new() {
        let inner_error = std::io::Error::new(std::io::ErrorKind::NotFound, "file not found");
        let io_error = IoError::new("File Operation", inner_error);

        println!("{}", io_error);
        // 验证是否正确设置了上下文和内部错误
        assert_eq!(io_error.context, Some("File Operation".to_string()));
        assert_eq!(io_error.inner.kind(), std::io::ErrorKind::NotFound);
        assert_eq!(io_error.inner.to_string(), "file not found");
    }

    #[test]
    fn test_io_error_creation_without_context() {
        let inner_error = std::io::Error::new(std::io::ErrorKind::PermissionDenied, "permission denied");
        let io_error = IoError::without_context(inner_error);

        // 验证没有上下文
        assert_eq!(io_error.context, None);
        assert_eq!(io_error.inner.kind(), std::io::ErrorKind::PermissionDenied);
        assert_eq!(io_error.inner.to_string(), "permission denied");
    }

    #[test]
    fn test_display_io_error_with_context() {
        let inner_error = std::io::Error::new(std::io::ErrorKind::NotFound, "file not found");
        let io_error = IoError::new("File Operation", inner_error);

        let formatted = format!("{}", io_error);
        // 验证格式化输出是否符合预期
        assert_eq!(formatted, "File Operation: file not found");
    }

    #[test]
    fn test_display_io_error_without_context() {
        let inner_error = std::io::Error::new(std::io::ErrorKind::PermissionDenied, "permission denied");
        let io_error = IoError::without_context(inner_error);

        let formatted = format!("{}", io_error);
        // 验证没有上下文时的格式化输出
        assert_eq!(formatted, "permission denied");
    }

    #[test]
    fn test_io_error_invalid_data() {
        let io_error = IoError::invalid_data("File Operation", "invalid data encountered");

        // 验证创建的错误类型
        assert_eq!(io_error.context, Some("File Operation".to_string()));
        assert_eq!(io_error.inner.kind(), std::io::ErrorKind::InvalidData);
        assert_eq!(io_error.inner.to_string(), "invalid data encountered");
    }

    #[test]
    fn test_io_error_other() {
        let io_error = IoError::other("Network", "network failure");

        assert_eq!(io_error.context, Some("Network".to_string()));
        assert_eq!(io_error.inner.kind(), std::io::ErrorKind::Other);
        assert_eq!(io_error.inner.to_string(), "network failure");
    }
}

