/**
* Copyright(2024,)Institute of Software, Chinese Academy of Sciences
* author: jiangliuwei@iscas.ac.cn
* since: 0.1.0
*/
use std::time::Duration;
use reqwest::Client;
use serde_json::Value;
use tokio::time::sleep;
use crate::util::ioerror::{IoError, IoResult};

// todo:: 需要优化
async fn check_http_service(address: &str) -> IoResult<String> {
    let url = format!("{}/cluster/info", address); // 要访问的 URL
    let client = Client::new();
    let response = client.get(&url).send().await;

    match response {
        Ok(rep) => {
            if rep.status().is_success() {
                let body = rep.text().await.map_err(|e| {
                    IoError::without_other(format!("Failed to read response body: {}", e))
                })?;
                let json: Value = serde_json::from_str(&body).map_err(|e| {
                    IoError::without_other(format!("Failed to parse JSON: {}", e))
                })?;
                if let Some(data) = json.get("data").and_then(|v| v.as_object()) {
                    if let Some(cluster_id) = data.get("cluster_id").and_then(|v| v.as_str()) {
                        Ok(cluster_id.to_string())
                    } else {
                        Err(IoError::without_other("Missing 'cluster_id' in 'data' field"))
                    }
                } else {
                    Err(IoError::without_other("Missing or invalid 'data' field"))
                }
            } else {
                Err(IoError::without_context(std::io::Error::new(
                    std::io::ErrorKind::Other,
                    "Service not healthy",
                )))
            }
        }
        Err(e) => Err(IoError::without_other(format!("get http service failed {}", e))),
    }
}



pub async fn check_apiserver_service(address: &str) -> IoResult<String> {
    loop {
        match check_http_service(address).await {
            Ok(val) => {
                log::info!("connect apiserver succeed");
                return Ok(val);
            }
            Err(e) => {
                log::warn!("Failed to reach the service: {}", e);
            }
        }
        sleep(Duration::from_secs(5)).await;
    }
}



