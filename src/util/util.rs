use std::fs;
use std::path::Path;
use crate::COMPONENT;
use crate::util::ioerror::{IoError, IoResult};
use crate::util::log::init_logger;

pub async fn init_fleet_logger(dir_path: &str, level: &str) -> IoResult<()> {
    let path = format!("{}/log/{}", dir_path, COMPONENT);
    log::info!("log path {path}");
    init_logger(path.as_str(), level, COMPONENT)
        .map_err(|e| {
            log::error!("Failed to initialize logger: {}", e);
            IoError::interrupted("Logger initialization failed", &e.to_string())
        })
}

pub async fn init_dir(dir: &str) -> IoResult<()> {
    let base_path = Path::new(dir);
    if !base_path.exists() {
        log::info!("Directory {} does not exist. Creating...", dir);
        fs::create_dir_all(base_path).map_err(|e| IoError::without_context(e))?
    }

    let db_path = base_path.join("db");
    if !db_path.exists() {
        log::info!("Directory {} does not exist. Creating...", db_path.display());
        fs::create_dir_all(&db_path).map_err(|e| IoError::without_context(e))?
    }

    let log_path = base_path.join("log");
    if !log_path.exists() {
        log::info!("Directory {} does not exist. Creating...", log_path.display());
        fs::create_dir_all(&log_path).map_err(|e| IoError::without_context(e))?
    }

    Ok(())
}