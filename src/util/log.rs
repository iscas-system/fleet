/**
* Copyright(2024,)Institute of Software, Chinese Academy of Sciences
* author: jiangliuwei@iscas.ac.cn
* since: 0.1.0
*/
use std::fs;
use log::{LevelFilter};
use log4rs::{
    append::{
        console::ConsoleAppender,
        rolling_file::{
            RollingFileAppender,
            policy::compound::{
                CompoundPolicy,
                trigger::size::SizeTrigger,
            },
        },
    },
    config::{Appender, Config, Root},
    encode::pattern::PatternEncoder,
};
use log4rs::append::rolling_file::policy::compound::roll::fixed_window::FixedWindowRoller;

/// 清理旧的日志文件，仅保留指定数量的日志
fn clean_old_logs(log_dir: &str, keep_count: usize) -> Result<(), Box<dyn std::error::Error>> {
    let mut log_files: Vec<_> = fs::read_dir(log_dir)?
        .filter_map(Result::ok)
        .filter(|e| e.path().extension().and_then(|ext| ext.to_str()) == Some("log"))
        .collect();

    // 按文件名排序（假设文件名中包含时间信息，可以按名称顺序清理旧文件）
    log_files.sort_by_key(|entry| entry.path());

    // 删除多余的日志文件
    for entry in log_files.iter().take(log_files.len().saturating_sub(keep_count)) {
        fs::remove_file(entry.path())?;
    }

    Ok(())
}

/// 初始化日志配置
pub fn init_logger(log_dir: &str, log_level: &str, compent: &str) -> Result<(), Box<dyn std::error::Error>> {
    // 确保日志目录存在
    fs::create_dir_all(log_dir)?;

    // 清理旧日志文件，只保留最近 10 个
    clean_old_logs(log_dir, 10)?;

    // 日志文件路径模板
    let log_filename_pattern = format!("{}/{compent}_{{}}.log", log_dir);

    // 控制台输出配置
    let stdout = ConsoleAppender::builder()
        .encoder(Box::new(PatternEncoder::new(
            &format!("[{{d(%Y-%m-%d %H:%M:%S)}} {{level}} {{file}}:{{line}}] {{message}}\n")
        )))
        .build();

    // 日志文件大小限制
    let file_size_limit = 10 * 1024 * 1024; // 10MB

    // 使用 FixedWindowRoller 管理日志滚动
    let roller = FixedWindowRoller::builder()
        .build(&log_filename_pattern, 10)?; // 只保留最近 10 个日志文件

    let trigger = SizeTrigger::new(file_size_limit);

    let log_file = RollingFileAppender::builder()
        .encoder(Box::new(PatternEncoder::new(
            &format!("[{{d(%Y-%m-%d %H:%M:%S)}} {{level}} {{file}}:{{line}}] {{message}}\n")
        )))
        .build(
            format!("{}/{compent}.log", log_dir),
            Box::new(CompoundPolicy::new(Box::new(trigger), Box::new(roller))),
        )?;

    // 解析日志级别
    let log_level = match log_level.to_uppercase().as_str() {
        "INFO" => LevelFilter::Info,
        "WARN" | "WARNING" => LevelFilter::Warn,
        "ERROR" => LevelFilter::Error,
        "DEBUG" => LevelFilter::Debug,
        "TRACE" => LevelFilter::Trace,
        _ => LevelFilter::Info, // 默认级别
    };

    let config = Config::builder()
        .appender(Appender::builder().build("stdout", Box::new(stdout)))
        .appender(Appender::builder().build("log_file", Box::new(log_file)))
        .build(
            Root::builder()
                .appenders(vec!["stdout", "log_file"])
                .build(log_level),
        )?;


    log4rs::init_config(config)?;

    Ok(())
}
