# 定义目标
run: create_dir cp_plugins cp_libs update_links cargo_run
build:: create_dir cp_plugins cp_libs update_links cargo_build
# 创建目录
create_dir:
	mkdir -p /root/iscas/fleet/lib

# 移动插件到目标目录
cp_plugins:
	@for file in caching-plugin.so portal-plugin.so scheduling-plugin.so storage-plugin.so; do \
		if [ -f /root/iscas/fleet/lib/$$file ]; then \
			if ! cmp -s ./lib/$$file /root/iscas/fleet/lib/$$file; then \
				echo "Hash mismatch for $$file. Copying to /root/iscas/fleet/lib/"; \
				cp ./lib/$$file /root/iscas/fleet/lib/; \
			else \
				echo "Hash matches for $$file. Skipping."; \
			fi; \
		else \
			echo "File $$file not found in /root/iscas/fleet/lib/. Copying..."; \
			cp ./lib/$$file /root/iscas/fleet/lib/; \
		fi; \
	done

# 移动库文件到 lib 目录
cp_libs:
	@for file in libfleet-datamgr.so libnats.so.3.10.0; do \
		if [ -f ./lib/$$file ]; then \
			if ! cmp -s ./lib/$$file /lib/$$file; then \
				echo "Hash mismatch for $$file. Copying to /lib/"; \
				cp ./lib/$$file /lib/; \
			else \
				echo "Hash matches for $$file. Skipping."; \
			fi; \
		else \
			echo "File $$file not found in /lib/. Copying..."; \
			cp ./lib/$$file /lib/; \
		fi; \
	done

# 执行 ldconfig
update_links:
	ldconfig

# 执行 cargo run，并支持参数传递
cargo_run:
	NODE_NAME=node1 cargo run start core -- $(ARGS)

# 执行 cargo build
cargo_build:
	cargo build --release


# 执行卸载脚本
uninstall:
	@if [ -f ./script/uninstall.sh ]; then \
		echo "Running uninstall script..."; \
		./script/uninstall.sh; \
	else \
		echo "Uninstall script not found!"; \
	fi

.PHONY: run create_dir cp_plugins cp_libs update_links cargo_run cargo_build
