#!/bin/bash

#查找包含 "fleet start core" 的进程
echo "Searching for process: 'fleet start core'..."
PROCESS=$(ps aux | grep "fleet start core" | grep -v grep)

if [ -z "$PROCESS" ]; then
    echo "No process found matching 'fleet start core'."
else
    # 输出找到的进程信息
    echo "Found process:"
    echo "$PROCESS"

    # 提取进程 ID (PID)
    PID=$(echo "$PROCESS" | awk '{print $2}')

    # 杀死进程
    echo "Killing process with PID: $PID..."
    kill -9 "$PID"
fi

systemctl stop fleetnats
systemctl stop fleet

# 删除指定目录
echo "Deleting directories..."
rm -rf /root/iscas/
rm -rf /root/control.db
rm -rf /root/web/
rm -rf /root/location1
rm -rf /root/location2
rm -rf /root/location3
rm -rf /nats.log
rm -rf /jetstream
echo "Directories deleted."

# 删除网络接口
echo "Deleting network interface 'hostSideIface'..."
ip link del hostSideIface 2>/dev/null || echo "Network interface 'hostSideIface' does not exist or failed to delete."

# 停止并删除所有容器
echo "Stopping and removing all containers..."
crictl ps -q | xargs -r crictl stop
crictl ps -qa | xargs -r crictl rm
echo "Containers stopped and removed."

# 停止并删除所有 Pod
echo "Stopping and removing all pods..."
crictl pods -q | xargs -r crictl stopp
crictl pods -q | xargs -r crictl rmp
echo "Pods stopped and removed."

echo "Cleanup completed."
