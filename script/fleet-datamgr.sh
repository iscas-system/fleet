#!/bin/bash

# 上传文件
curl --location 'localhost:8080/data/self/file' \
--header 'x-data-file-name: test.txt' \
--header 'x-data-file-description: this is a description for test.txt' \
--header 'Content-Type: application/octet-stream' \
--data-binary '@/C:/Users/dell/Desktop/id_rsa' # 请在'@/' 后添加正确的文件路径

# 上传状态
curl --location --request POST 'localhost:8080/data/other/status' \
--header 'x-data-status: This is a Status Msg'

# 上传指令
curl --location --request POST 'localhost:8080/data/user/instruction' \
--header 'x-data-instruction: This is an Instruction Msg'

# 下载文件
curl --location 'localhost:8080/data/self/file' \
--header 'x-data-file-name: test.txt'

# 列出文件列表
curl --location 'localhost:8080/data/self/files'

# 查询文件信息
curl --location 'localhost:8080/data/self/fileinfo' \
--header 'x-data-file-name: test'

# 一般吞吐数据转收
curl --location 'http://localhost:8080/data/v1/normal' \
	--header 'x-data-key: testKey' \
	--header 'x-data-size: 5' \
	--header 'Content-Type: text/plain' \
	--data 'hello'

# 数据同步和删除策略设置
curl --location --request POST 'http://1ocalhost:8080/data/v1/strategy' \
	--header 'x-data-key: testKey' \
	--header 'x-data-sync-strategy: default' \
	--header 'x-data-recycle-time: 2025-01-12' \
	--header 'x-data-recycle-cascade-keys: key-dep1,key-dep2'

# 提交用户需求
curl --location 'http://localhost:8080/data/v1/request' \
	--header 'x-data-key: testKey' \
	--header 'x-data-size: 5' \
	--header 'Content-Type: text/plain' \
	--data 'hello'

# 情报数据查询 - 查询数据位置
curl --location --head 'http://localhost:8080/data/v1/intelligence' \
	--header 'x-data-key: testKey'

# 情报数据查询 - 下载具体数据
curl --location 'http://localhost:8080/data/v1/intelligence' \
	--header 'x-data-key: testKey' \
	--header 'x-data-size: 5'

# 终端连接信息恢复 - 终端连接
curl --location 'http://localhost:8080/data/v1/terminal' \
	--header 'x-data-user-token: ppnn13%dkstFeb.1st' \
	--header 'x-data-user-data-size: 5' \
	--header 'Content-Type: text/plain' \
	--data 'hello'

# 终端连接信息恢复 - 连接信息恢复
curl --location 'http://localhost:8080/data/v1/terminal' \
	--header 'x-data-user-token: ppnn13%dkstFeb.1st' \
	--header 'x-data-user-data-size: 5'

